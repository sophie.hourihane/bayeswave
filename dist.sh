#!/bin/bash
#
# Script to build a source distribution for BayesWave
# based on a git reference (ideally a tag).
#
# Author: Duncan Macleod <duncan.macleod@ligo.org>
#

set -e

# get the TAG from the command-line arguments
TAG=$1
shift 1 || {
  echo "Please include tag to build."; 1>&2
  echo; 1>&2
  echo "usage: bash dist.sh {TAG}"; 1>&2
  exit 1;
}
# NOTE: all other arguments are passed to the
#       CMake configure command

# clone the project into a temporary directory, to ensure a clean copy
_TMPDIR="$(mktemp -d)"
trap "rm -rf ${_TMPDIR}" EXIT
GIT_URL="https://git.ligo.org/lscsoft/bayeswave.git"
git clone \
  --quiet \
  --depth 1 \
  --branch "${TAG}" \
  ${GIT_URL} \
  "${_TMPDIR}" \
;

# build the tarball in a clean subdirectory
rm -rf dist
mkdir -p dist
cd dist
cmake \
  "${_TMPDIR}" \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  "$@" \
;
cmake \
  --build . \
  --target package_source \
  --verbose \
;
