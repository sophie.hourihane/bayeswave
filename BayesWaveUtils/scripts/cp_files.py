#!/usr/bin/env python
import shutil, sys, os, subprocess
from bayeswave_plot import BW_Flags as bwf

def keep_which_directory(temp, local):
    # read in from each what model is currently being run, and decide from there which one to keep
    # If one is in the cleaning stage, while the other is not, it will keep the one not being cleaned
    # if the 2 are in the same state, it will keep the version with more iterations

    # returns winner, loser

    # Read in trigger directory name
    trigdir =  str(sys.argv[1])
    runFlag_local = bwf.Flags(local)
    runFlag_temp = bwf.Flags(temp)

    runFlag_dict = {'temp':runFlag_temp, 
                    'local':runFlag_local}
    dirs = { 'temp'  : temp, 
             'local' : local}
    #dirs = [temp, local]

    # If temp bayeswave.run doesn't exist, then just keep the local version
    # if local bayeswave.run doesn't exist (and temp does), then keep the temp version!
    for i, key in enumerate(runFlag_dict.keys()):
        try:
            bwf.readbwb(runFlag_dict[key])
        except FileNotFoundError as e:
            print(e)
            # if we did not find the bayeswave.run file, then choose the other key as the winner
            print("WARNING: " + runFlag_dict[key].trigdir + '/bayeswave.run' + " has yet to be created")
            if key == 'local':
                # winner, loser 
                return dirs['temp'], dirs['local']
            return dirs['local'], dirs['temp']

    # checkpointing isn't even on, so there is no reason to check checkpointing files, exiting this function
    if not runFlag_dict['local'].checkpoint:
        sys.exit("--checkpoint has not been turned on, returning")

    models = {'temp':None, 'local':None}
    lengths = {}

    # now actually read in the model and iterations
    for i, key in enumerate(runFlag_dict.keys()):
        ### Read in model 
        try:
            # read in the model name
            f = open(runFlag_dict[key].trigdir + '/checkpoint/state.dat')
        except FileNotFoundError:
            print("FileNotFound in {0}/checkpoint/state.dat".format(runFlag_dict[key].trigdir))
            # if checkpointing file not found, then return the other directory
            if key == 'local':
                # winner, loser 
                return dirs['temp'], dirs['local']
            return dirs['local'], dirs['temp']
        # read in model from first word of first line in the state file
        model = str(f.readlines()[0].split()[0])
        models[key] = model
        f.close()


        ### Read in iterations 
        try:
            f = open(runFlag_dict[key].trigdir + '/checkpoint/temperature.dat')
        except FileNotFoundError:
            # if checkpointing file not found, then return the other directory
            if key == 'local':
                # winner, loser 
                return dirs['temp'], dirs['local']
            return dirs['local'], dirs['temp']

        # Keep track of file length 
        file_length  = int(f.readlines()[0]) // runFlag_dict[key].Ncycle
        lengths[key] = file_length
        f.close()

    # now we confirmed we have 2 directories that both have stuff in them, let's decide which one is more full.
    if models['temp'] == models['local']:
        # the 2 models are the same, return the directory corresponding to the larger checkpoint
        if lengths['temp'] > lengths['local']:
            return dirs['temp'], dirs['local']
        # if they are the same length, keep the local directory 
        return dirs['local'], dirs['temp']
    # the 2 models are different, prioritize whichever one is not currently in the cleaning phase
    if models['local'] == 'clean':
        return dirs['temp'], dirs['local']
    if models['temp'] == 'clean':
        return dirs['local'], dirs['temp']

    # when in doubt, just keep the local directory
    # winner, loser
    return dirs['local'], dirs['temp']


def switch_and_delete_dirs(winner, loser, dirname):
    # winner becomes dirname, loser gets deleted

    # To make sure we don't delete something important, we make sure that the loser directory
    # is something that we actually do want to delete
    # directories we are deleteing should be of the form: WHATEVER/trigtime_WHATEVER or temp
    deletenames = ['trigtime', 'temp']
    delete_loser = False
    for delete in deletenames:
        if delete in loser:
            delete_loser = True
    if not delete_loser:
        print("I AM NOT DELETING THE {loser} DIRECTORY!".format(loser = loser))
        print("ERROR switch_and_delete_dirs: name does not contain temp or trigtime, returning")
        return
    else:
        # loser gets deleted
        shutil.rmtree(loser)
    # winner gets moved
    shutil.move(winner, dirname)
    return

# Make sure that directory string does not have a trailing '/'
def clean_input(dir_string):
    if dir_string[-1] == '/':
        return dir_string[:-1]
    else:
        return(dir_string)

# Call to cp_files is 
#       cp_files.py local temp

workdir = os.getcwd()
print("cwd is {0}".format(workdir))

# read in the directory local directory, then we make a temp directory from that directory
# copy over the working directory to the remote machine
tempdir =  clean_input(str(sys.argv[2]))
temp = './temp'
#shutil.copytree(tempdir, temp)
print("Copying over files from tempdir {0} to tempdir {1}".format(tempdir, temp))
p = subprocess.Popen('cp -r {0} {1}'.format(tempdir, temp), shell = True,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
p.wait()

# localdir
outputdir = clean_input(str(sys.argv[1]))
print("localdir is {0}".format(outputdir))
try:
    process = subprocess.run('head {0}/checkpoint/temperature.dat'.format(outputdir), shell = True)
except:
    pass

winner, loser = keep_which_directory(temp, outputdir)
print("Keeping winner {0} \n Deleting loser {1}".format(winner, loser))
switch_and_delete_dirs(winner, loser, outputdir)
