# BayesCBC submodule
This page describes how the BayesCBC module for BayesWave.


### Output files

- `chains/cbc_model.dat.<i>` where i is the chain number (BW prints cold chain
0 by default, with verbose flags chain files for all chains are printed)
- `waveforms/*` CBC waveforms
