# Quick Start Guide

## Where is the software
BayesWave has been deployed to the IGWN conda environments, LSCSoft package repositories and singularity since this documentation was written.

Unless you are actively developing code or require a feature not yet in a production release, please use the IGWN conda deployments.  E.g. update your `config.ini` to:
```
[engine]
install_path=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin
bayeswave=%(install_path)s/BayesWave
bayeswave_post=%(install_path)s/BayesWavePost
megaplot=%(install_path)s/megaplot.py
megasky=%(install_path)s/megasky.py
```


## Install BayesWave


Use this if you're too busy to read / follow the sprawling instructions at:

 * [Installation](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/INSTALLATION.md)
 * [Running Analyses](https://git.ligo.org/lscsoft/bayeswave/blob/master/doc/RUNNING.md)


Assume bayeswave install path: `${HOME}/opt/lscsoft/bayeswave`:

```
export BAYESWAVE_REPO=${HOME}/src/lscsoft/bayeswave
export BAYESWAVE_PREFIX=${HOME}/opt/lscsoft/bayeswave
git clone git@git.ligo.org:lscsoft/bayeswave.git ${BAYESWAVE_REPO}
cd bayeswave
./install.sh ${BAYESWAVE_PREFIX}
```

This configures, builds and installs *all* executables and libraries (i.e.,
including python) to ${BAYESWAVE_PREFIX}.  Upon successful installation, you
should see a message like:
```
*****************************************************************************
  DONE: BayesWave built and installed to: 
      /home/albert.einstein/opt/lscsoft/bayeswave
  To use: 
      source /home/albert.einstein/opt/lscsoft/bayeswave/etc/bayeswave-user-env.sh
*****************************************************************************
```
Source that file per the instructions and your environment will be ready to go.

## Analyze GW150914 with your own installation

Copy the example ini file which was installed to BAYESWAVE_PREFIX (defined above):
 
```
cp ${BAYESWAVE_PREFIX}/lib/python2.7/site-packages/bayeswave_pipe_examples/LDG-GW150914/LDG-GW150914.ini .
```

Modify paths in the `[engine]` section to point to your installation:

```
sed -i "s|BAYESWAVE_PREFIX|${BAYESWAVE_PREFIX}|g" LDG-GW150914.ini
```
Make sure you've sourced the `bayeswave-user-env.sh` script as described above
and run the pipeline to set up an analysis of a single trigger time:

 ```
 bayeswave_pipe LDG-GW150914.ini \
    --trigger-time 1126259462.420000076 \
    --workdir LDG-GW150914
 ```

That's it!  To start the analysis, simply follow the on-screen prompt:

```
    To submit:
        cd LDG-GW150914
        condor_submit_dag bayeswave_LDG-GW150914.dag
```

**WARNING**: the configuration file in this example is designed to produce a
*fast* end-to-end run for the purposes of illustration and workflow testing.
It does *not* produce a meaningful science result.   Please comment out the
`[bayeswave]` configuration lines to revert to default values.






